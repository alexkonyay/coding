from random import random 


def encode(bit_string):
    return "".join([i*3 for i in bit_string])


def noise(bit_string):
    new_bit_array = []
    for i in range(len(bit_string)):
        if random() < 0.1:
            print("Испорчен {0} бит в сообщении".format(i))
            new_bit_array.append("1" if bit_string[i] == "0" else "0")
        else:
            new_bit_array.append(bit_string[i])
    return "".join(new_bit_array)


def denoise(bit_string):
    i = 0
    bit_array = []
    while i < len(bit_string):
        bit_array.append(bit_string[i] + bit_string[i+1] + bit_string[i+2])
        i += 3
    
    normalized_array  = []
    for triad in bit_array:
        counter_of_1 = 0
        for symbol in triad:
            if symbol == "1":
                counter_of_1 += 1
        if counter_of_1 >= 2: normalized_array.append("1")
        else: normalized_array.append("0")
    return "".join(normalized_array)

    


if __name__ == '__main__':
    test_message = "101010101010101010101010101010"
    encoded_message = encode(test_message)
    noised_message = noise(encoded_message)
    denoised_message = denoise(noised_message)
    print("Исходное сообщение: " +test_message)
    print("Закодированное сообщение: " + encoded_message)
    print("Испорченое     сообщение: " + noised_message)
    print("Восстановленое сообщение: " + denoised_message)