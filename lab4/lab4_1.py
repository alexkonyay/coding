class Tree:
    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right
    
    def __str__(self):
        return "({0}, {1})".format(str(self.left), str(self.right))

def str_to_frequency_dict(string):
    result = dict()
    for i in string:
        if i in result.keys():
            result[i] += 1
        else:
            result[i] = 1
    return result

def find_in_dict(dictionary: dict, value):
    """
    Ищет в словаре ключ по заданному значению.
    Возвращает первое вхождение, если совподений нет - None
    """
    for key, val in dictionary.items():
        if val is value:
            return key
    print("значение {0} не найдено в словаре {1}".format(value, dictionary))
    return None

def find_and_remove_min_in_dict(dictionary: dict):
    """
    Ищет и удаляет элемент с минимальным значеним в словаре
    Возвращает кортеж (key, value) удаленного элемента 
    """
    if len(dictionary) == 0: return
        
    value = min(dictionary.values())
    key = find_in_dict(dictionary, value)
    dictionary.pop(key)
    return (key, value)


def tree_of_min_values(dictionary:dict, tree=None):
    """
    Находит два минимальных значения в словаре,
    удаляет эти значения,
    создает простое дерево из их ключей,
    добавляет его в словарь и возвращает
    """
    left = find_and_remove_min_in_dict(dictionary)
    right = find_and_remove_min_in_dict(dictionary)
    tree = Tree(left[0], right[0])
    dictionary[tree] = left[1]+right[1]
    return tree


def dict_to_tree(dictionary: dict):
    """
    Добавляет все ключи из словаря в дерево
    начиная с наименьшего значения
    """
    result = None
    while len(a) > 1:
        result = tree_of_min_values(dictionary)
    return result


def tree_to_dict_of_codes(tree:Tree, codes = None, current_code=''):
    if codes is None: codes = {}

    if type(tree.left) is type(Tree()):
        tree_to_dict_of_codes(tree.left, codes, current_code + "0")
    if type(tree.right) is type(Tree()):
        tree_to_dict_of_codes(tree.right, codes, current_code + "1")
    
    if type(tree.left) is type(''):
        codes[tree.left] = current_code + "0"
    if type(tree.right) is type(''):
        codes[tree.right] = current_code + "1"

def messages_to_code(message, codes):
    result = ''
    for i in message:
        result += codes[i]
    return result

def decode_message_by_tree(code, codes_tree: Tree):
    result = ''
    current_tree = codes_tree
    while len(code) > 0:
        current_symbol = code[0]
        code = code[1:]
        if str(current_symbol) is '0':
            # Если 0, идем налево
            if type(current_tree.left) is type(''):
                result += current_tree.left
                current_tree = codes_tree
                continue
            elif type(current_tree.left) is type(Tree()):
                current_tree = current_tree.left
                continue
        elif str(current_symbol) is '1':
            # Если 1, идем направо
            if type(current_tree.right) is type(''):
                result += current_tree.right
                current_tree = codes_tree
            elif type(current_tree.right) is type(Tree()):
                current_tree = current_tree.right
                continue
    return result


if __name__ == "__main__":
    test_message = "quick red fox jump aaaaaaaaaaaaaaaaover the lazy dog"
    # test_message = "qwertyuiopasdfghjklzxcvbnm"
    a = str_to_frequency_dict(test_message)
    print(a)
    tree = dict_to_tree(a)
    print(tree)
    codes = {}
    # Костыль: функция заполняет словарь codes значениями
    tree_to_dict_of_codes(tree, codes, '')
    print(codes)
    encoded_message = messages_to_code(test_message, codes)
    print(encoded_message)
    decoded_message = decode_message_by_tree(encoded_message, tree)
    print(decoded_message)