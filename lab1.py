import math


def file_to_string(file):
    return open(file, "r").read()


def dict_of_symbols(text):
    symbols = dict()
    for i in text:
        if i in symbols:
            symbols[i] += 1
        else:
            symbols[i] = 1
    return symbols


def alphabet_size(dictionary):
    return len(dictionary)


# Объемный подход
def information_count(text, encoding_length=8):
    return len(text) * encoding_length


# Равновероятностный подход
def equiprobable_approach(text): 
    dictionary = dict_of_symbols(text)
    return math.log2(len(dictionary)) * len(text)


# Неравновероятныстный подход
def unequiprobable_approach(text):
    result = 0
    dictionary = dict_of_symbols(text)
    size = alphabet_size(dictionary)
    for key, value in dictionary.items():
        p_i = value/len(text)
        result += p_i * math.log2(1/p_i)
    return result * len(text)


def main():
    text = file_to_string("lorem.txt")
    dictionary = dict_of_symbols(text)
    print(dictionary, "\n")
    print(len(dictionary), "\n")
    print("Алфавитный подход\n", information_count(text), "\n")
    print("Равновероятностный подход\n", equiprobable_approach(text), "\n")
    print("Неравновероятностный подход\n", unequiprobable_approach(text))


if __name__ == '__main__':
    main()