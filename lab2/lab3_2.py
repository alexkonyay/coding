def split_array(array):
    i = 0
    bit_array = []
    while i < len(array):
        bit_array.append(array[i:i+7])
        i += 7
    return bit_array

def add_even_bit(array_of_seven_bit):
    counter = 0
    for i in array_of_seven_bit:
        if str(i) == "1":
            counter += 1
    if counter % 2 == 1:
        s = array_of_seven_bit + "1"
    else:
        s = array_of_seven_bit + "0"
    return s


def main():
    splited_string = split_array("101010101010101010101010101010")
    evenized_splited_string = [add_even_bit(i) for i in splited_string]
    print(splited_string)
    print(evenized_splited_string)


if __name__ == '__main__':
    main()