import math


def get_internal_representation(signed_integer):
    if not (-128 < signed_integer < 128):
        print("illegal argument: {0}".format(signed_integer))
        return
    # переводим в двоичный вид и избавляемся от знака
    unsigned_bin = bin(int(math.fabs(signed_integer)))
    # избавляемся от 0b
    bin_list = [i for i in str(unsigned_bin)[2:]]
    # создаем массив из восьми символов путем сложения 
    # двух массивов: пустого и с бинарным представлением
    result = [0] * (8 - len(bin_list)) + bin_list
    # если число отрицательное, добавляем знак
    if signed_integer < 0: result[0] = 1
    return result


def get_internal_representation_additional_code(signed_integer):
    if not (-128 < signed_integer < 128):
        print("illegal argument: {0}".format(signed_integer))
        return
    if(signed_integer >= 0):
        return get_internal_representation(signed_integer)
    else:
        new_integer = -signed_integer - 1
        new_array = get_internal_representation(new_integer)
        result = []
        for i in new_array:
            result.append('1') if str(i) == '0' else result.append('0')
        return result