from internal_representation import get_internal_representation, get_internal_representation_additional_code
import converters as c

test_text = "Hello world"
test_num = -127

ascii = c.str_to_ascii(test_text)
biary = c.ascii_to_binary(ascii)
hex = c.ascii_to_hex(ascii)
string = c.ascii_to_str(ascii)

print("Исходный текст:\n" + str(test_text))
print("Кодировка ASCII:\n" + str(ascii))
print("Бинарное представление:\n" + str(biary))
print("Шестнадцатиричное представление:\n" + str(hex))
print("Обратно в строку:\n" + str(string))

print("Внутреннее представление числа {0}:\n{1}".format(
    test_num,
    get_internal_representation_additional_code(test_num)))
