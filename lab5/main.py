from random import randint


def encode_caesar(message:bytearray, shift:int):
    return [(i+shift)%256 for i in message]

def decode_caesar(message:bytearray, shift:int):
    return [(i-shift)%256 for i in message]

def encode_xor(message:bytearray, key:bytearray = None):
    if not key is None: return [message[i]+key[i%len(key)] for i in range(len(message))]
    key = bytearray([randint(0, 255) for i in range(4)])
    return key, encode_xor(message, key)

def decode_xor(message:bytearray, key:bytearray):
    return [message[i]-key[i%len(key)] for i in range(len(message))]

def encode_transposition(message:str):
    """
    This is the only one cipher that work with str instead of byte[]
    due to python can easily interact with bits of str, but work with
    bits of byte is terrible. 

    Transposition rule:
    1 <-> 3, then 4 <-> 7
    But for python bin('a') is 0b1100001,
    And indexing starts with 0,
    so whe have to shift all indexes before 1.
    Correct transposition rule:
    2 <-> 4, then 5 <-> 8
    """
    result = ""
    for symbol in message:
        bin_symbol = bin(ord(symbol))
        # bin_symbol may be shorter than 8 bits
        bin_symbol = bin_symbol[:2] + '0'*(9-len(bin_symbol)) + bin_symbol[2:]
        new_symbol = bin_symbol[0] + \
                     bin_symbol[1] + \
                     bin_symbol[4] + \
                     bin_symbol[3] + \
                     bin_symbol[2] + \
                     bin_symbol[8] + \
                     bin_symbol[6] + \
                     bin_symbol[7] + \
                     bin_symbol[5]
        
        result += chr(int(new_symbol,2))
    return result

def decode_transposition(message:str):
    return encode_transposition(message)

if __name__ == "__main__":
    # caesar
    print(bytearray(b"abcd"))
    print(encode_caesar(bytearray(b"abcd"), 0))
    print(encode_caesar(bytearray(b"abcd"), 200))
    print(decode_caesar(encode_caesar(bytearray(b"abcd"), 200), 200))
    print()
    # xor
    print(encode_xor(bytearray(b"abcd"), bytearray(b"dcba")))
    print(decode_xor(encode_xor(bytearray(b"abcd"), bytearray(b"dcba")), bytearray(b"dcba")))
    print()
    # xor with random
    message = bytearray(b"abcd")
    key, encoded_message = encode_xor(message)
    print(str(encoded_message) + " + " + str(key))
    print(decode_xor(encoded_message, key))
    print()
    # transposition
    message = encode_transposition("123")
    print(message)
    print(decode_transposition(message))
