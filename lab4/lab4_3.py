# -*- coding: utf-8 -*-
def compress(uncompressed):
    """Compress a string to a list of output symbols."""

    # Создаем словарь, который изначально содержит в себе
    # все однобуквенные подстроки
    dictionary = {chr(i): chr(i) for i in range(256)}

    word = ""
    result = []
    for char in uncompressed:
        word_and_char = word + char
        if word_and_char in dictionary:
            # Если такая подстрока уже есть в словаре,
            # продолжаем ее наращивать 
            word = word_and_char
        else:
            # Как только данной подстроки в словаре нет
            # добавляем ее в словарь
            result.append(dictionary[word])
            # Присваеваем ей новый индекс
            dictionary[word_and_char] = len(dictionary)
            word = char

    # Output the code for word.
    if word:
        result.append(dictionary[word])
    
    return result


def decompress(compressed):
    """Decompress a list of output ks to a string."""
 
    dictionary = {chr(i): chr(i) for i in range(256)}
    result = []
    word = compressed.pop(0)
    result.append(word)
    for k in compressed:
        if k in dictionary:
            entry = dictionary[k]
        elif k == len(dictionary):
            entry = word + word[0]
        else:
            raise ValueError('Bad compressed k: %s' % k)
        result.append(entry)

        # Add word+entry[0] to the dictionary.
        dictionary[len(dictionary)] = word + entry[0]

        word = entry
    return "".join(result)


# How to use:
compressed = compress('TOBEORNOTTOBEORTOBEORNOT')
print (compressed)
decompressed = decompress(compressed)
print (decompressed)