def gcd(a, b):
    """
    Greatest common divisor
    """
    if b > a:
        if b % a == 0:
            return a
        else:
            return gcd(b % a, a)
    else:
        if a % b == 0:
            return b
        else:
            return gcd(b, a % b)


def find_d(phi_n,e):
    """
    Вычисление закрытого ключа 
    """
    k = 1
    while True:
        d, rem = divmod(k*phi_n+1, e)
        if rem == 0:
            return d
        k+=1


def find_e(phi_n):
    """
    Вычисление открытого ключа 
    """
    e = 3
    while True:
        if not gcd(e,phi_n) == 1:
            e+=2
        else:
            return e

def generate_keys(p, q):
    # Модуль
    n = p*q
    # Функция Эйлера от числа n
    phi_n = (p-1)*(q-1)
    # Открытая экспонента
    e = find_e(phi_n)
    # Секретная экспонента
    d = int(find_d(phi_n, e))
    # (открытый ключ, закрытый ключ)
    return (e,n), (d,n)

def encrypt(message, key):
    return pow(message, key[0], key[1])

def decrypt(message, key):
    return pow(message, key[0], key[1])

def hash_(message):
    result = [ord(message[i]) * (256**i) for i in range(len(message))]
    return sum(result)

if __name__ == "__main__":
    message = "1010101"
    public_key, private_key = generate_keys(37975227936943673922808872755445627854565536638199, 40094690950920881030683735292761468389214899724061)
    h = hash_(message)
    encrypted_hash = encrypt(h, private_key)
    message_pair = (message, encrypted_hash)
    decrypted_hash = decrypt(message_pair[1], public_key)
    result = hash_(message_pair[0]) == decrypted_hash
    print(result)