# rsa
# see https://ru.wikipedia.org/wiki/RSA#Алгоритм_создания_открытого_и_секретного_ключей

def gcd(a, b):
    """
    Greatest common divisor
    """
    if b > a:
        if b % a == 0:
            return a
        else:
            return gcd(b % a, a)
    else:
        if a % b == 0:
            return b
        else:
            return gcd(b, a % b)


def find_d(phi_n,e):
    """
    Вычисление закрытого ключа 
    """
    k = 1
    while True:
        d, rem = divmod(k*phi_n+1, e)
        if rem == 0:
            return d
        k+=1


def find_e(phi_n):
    """
    Вычисление открытого ключа 
    """
    e = 3
    while True:
        if not gcd(e,phi_n) == 1:
            e+=2
        else:
            return e

def generate_keys(p, q):
    # Модуль
    n = p*q
    # Функция Эйлера от числа n
    phi_n = (p-1)*(q-1)
    # Открытая экспонента
    e = find_e(phi_n)
    # Секретная экспонента
    d = int(find_d(phi_n, e))
    # (открытый ключ, закрытый ключ)
    return (e,n), (d,n)

def encrypt(message, key):
    return pow(message, key[0], key[1])

def decrypt(message, key):
    return pow(message, key[0], key[1])


if __name__ == "__main__":
    # public_key, private_key = generate_keys(1997, 2011)
    public_key, private_key = generate_keys(23, 11)
    message = 100500
    encripted_message = encrypt(message, public_key)
    decripted_message = decrypt(encripted_message, private_key)
    print(public_key)
    print(private_key)
    print(encripted_message)
    print(decripted_message)