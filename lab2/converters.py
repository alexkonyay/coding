import math


def str_to_ascii(string):
    return [ord(i) for i in string]

def ascii_to_binary(array):
    return [bin(i) for i in array]

def ascii_to_hex(array):
    return [hex(i) for i in array]

def ascii_to_str(array):
    return [chr(i) for i in array]