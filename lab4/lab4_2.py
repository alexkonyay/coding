#-*- coding: utf8 -*-
from math import floor, log, ceil


class Arithmetic:
    def __init__(self, frequences_tuples, MAX):
        # верхняя граница интервала
        # использьуем вместо десятичных дробей
        self.max = MAX
        # подготовка словаря с интервалами вероятностей символов
        self.frequences = self.frequences_to_dict(frequences_tuples)
        


    def encode(self, string):
        # начинает с полного интервала [0,1)
        (left_bound, right_bound) = (0, self.max)
        for char in string:
            # координаты нового интервала
            (inner_left_bound, inner_right_bound) = self.frequences[char]
            # длина базового интервала
            interval = right_bound - left_bound
            # Получение нового интервала.
            right_bound = int(floor(
                (inner_right_bound * interval) / self.max + \
                left_bound
            ))
            left_bound = int(floor(
                (inner_left_bound * interval) / self.max + \
                left_bound
            ))
        # return self.__optimize( left_bound, right_bound )
        return left_bound + 1


    def decode(self, code):
        """
            Декодирование числа в строку на основе вероятностей в self.frequences
        """
        result = []
        current_interval = None
        while current_interval != '#':
            current_interval = self.__find_interval(code)
            result.append(current_interval)
            (left_bound, right_bound) = self.frequences[current_interval]
            code = int(
                (code - left_bound) * self.max / \
                (right_bound - left_bound))
        return ''.join(result)


    def frequences_to_dict(self, frequences_tuples):
        """
        Преобразование кортежа вероятностей в
        словарь интервалов с учетом self.max
        # {'#': (0, 50)}
        """
        freqs = {}
        left_bound = 0
        for (key, value) in frequences_tuples.items():
            freqs[key] = (int(left_bound*self.max), int((left_bound+value)*self.max))
            left_bound += value
        return freqs



    def __find_interval(self, value):
        """
            Ищем интервал, которому принадлежит значение v
        """
        for (key, (left, right)) in self.frequences.items():
            if value>=left and value<right:
                return key


def find_frequences(string:str):
    result = {}
    for i in string:
        if i in result.keys():
            result[i] += 1
        else:
            result[i] = 1

    result = {key: (value/len(string)) for (key, value) in result.items()}
    return result

if __name__ == "__main__":
    src = '010100110011001101010101001011101010101110100110#'
    frequences = find_frequences(src)
    MAX = 100000000
    coder = Arithmetic(frequences, MAX)
    print('SOURCE:  ', src)
    enc = coder.encode( src )
    dec = coder.decode( enc )
    print('DECODED: ', dec)
    print('ENCODED: ', enc)

