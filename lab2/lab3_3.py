import math

def array_of_pow_2(length):
    return [math.pow(2, i) for i in range(length)]

def control_bits_count(message_length):
    counter = 0
    pow2 = 1
    while pow2 <= message_length + 1:
        pow2 *= 2
        counter += 1
    return counter

def append_to(array, index, element):
    return array[:index] + [element] + array[index:]

def remove_index(array, index):
    return array[:index] + array[index+1:]

def get_pows_decomposition(number):
    # возвращает разложение числа по степеням двойки
    # # 7 = 4 + 2 + 1
    result = []
    temp = 0
    while number != 0:
        temp = 2 ** math.floor(math.log(number, 2))
        result.append(temp)
        number -= temp
    return result

def calculate_control_bit(array, index):
    result = 0
    counter = -1
    control_bit_positions = get_control_bits_position(array)
    for i in array:
        counter += 1
        if counter in control_bit_positions: continue
        if int(i) == 0: continue
        
        # поправка на индексацию с нуля: 
        # # 5 элемент имеет индекс 4
        # # разложение 4 = [4]
        # # но нам требуются элементы [1, 4] с индексами [0, 3]
        temp = [j-1 for j in get_pows_decomposition(counter+1)]
        if (index) in temp:
            result ^= 1
    return result

def get_control_bits_position(message):
    # returns 0, 1, 3, 7, 15, ...
    positions = [i-1 for i in array_of_pow_2(control_bits_count(len(message)))]
    return positions

def insert_X_control_bits(array):
    # вставляет X на место контрольных битов
    positions = get_control_bits_position(array)
    for i in positions:
        array = append_to(array, int(i), "X")
    return array

def encode(message):
    message_array = [i for i in message]
    message_with_x_in_control_bits = insert_X_control_bits(message_array)
    for i in range(len(message_with_x_in_control_bits)):
        if message_with_x_in_control_bits[i] != 'X': continue
        message_with_x_in_control_bits[i] = calculate_control_bit(message_with_x_in_control_bits, i)
    return message_with_x_in_control_bits

def decode(message):
    control_bits_positions = get_control_bits_position(message)
    current_control_bits = [int(message[int(i)]) for i in control_bits_positions]
    print("Текущие контрольные биты:\n" + str(current_control_bits))
    expected_control_bits = []
    for i in control_bits_positions:
        expected_control_bits.append(calculate_control_bit(message, i))
    print("Ожидаемые контрольные биты:\n" + str(expected_control_bits))
    if is_valid(current_control_bits, expected_control_bits):
        print("сообщение не содержит ошибок")
    else:
        wrong_bit = find_wrong_bit(current_control_bits, expected_control_bits)
        print("В сообщении ошибка на " + str(wrong_bit) + " бите")
        message[wrong_bit] = 0 if int(message[wrong_bit]) == 1 else 1   
    message = clean_message(message)
    return message
        
def clean_message(message):
    # здесь список индексов переворачивавется, чтобы быстро удалить все 
    # контрольные биты из сообщения
    # # [0,1,3] -> [3,2,0] 
    control_bits_positions = get_control_bits_position(message)
    control_bits_positions.reverse() 
    for i in control_bits_positions: message = remove_index(message, int(i))
    return message

def find_wrong_bit(current_control_bits, expected_control_bits):
    index = 0
    for i in range(len(current_control_bits)):
        if current_control_bits[i] != expected_control_bits[i]:
            index += (2**i)
    # поправка на индексацию с 0
    return index - 1

def is_valid(current_control_bits, expected_control_bits):
    for i in range(len(current_control_bits)):
        if int(current_control_bits[i]) != int(expected_control_bits[i]): return False
    return True

def main():
    # message = "0100010000111101"
    message = "010"
    print("Сообщение")
    print(message)
    test_coded_message = "100110"
    encoded_message = encode(message)
    print("Ожидаемое число с контрольными битами:")
    print(test_coded_message)
    print("Полученное число с контрольными битами:")
    print("".join([str(i) for i in encoded_message]))
    print("".join([str(i) for i in decode(encoded_message)]))
    print(message)
    print()
    print("Делаем ошибку в 1 бите")
    wrong_test_message = [i for i in test_coded_message]
    wrong_test_message[1] = ("1" if str(test_coded_message[1]) == "0" else "1")
    print("".join(wrong_test_message))
    print("".join([str(i) for i in decode(wrong_test_message)]))
    print(message)

    

if __name__ == '__main__':
    main()
    